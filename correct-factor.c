#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <pthread.h>
#include <gmp.h>
#include <mpfr.h>

#define N_THREADS 8 // Numero di threads da avviare per il calcolo
#define DEFAULT_PREC 256 // Precisione in bit

typedef struct {
    mpfr_srcptr a, l;
    int (*f)(mpfr_t res, const mpfr_srcptr x, const va_list params);
    int subdivisions, subdivision_start, subdivision_end;
    va_list params;
} args_t;

// Integratore col metodo dei trapezi
void * trapezi_hlp(void * vargs) {
    args_t args;
    memcpy(&args, vargs, sizeof(args)); // Eseguo sullo stack una copia verbatim degli argomenti
    va_list pass_params; // Temporanea per i parametri costanti
    mpfr_set_default_prec(DEFAULT_PREC); // Devo reimpostare la precisione di default per ogni thread avviato

    mpfr_t x, y, inc; // temporanei per le x dove valutare f, la f(x), e gli incrementi
    mpfr_inits(x, y, inc, NULL); // Inizializzo tutto
    mpfr_t sum; // Accumulatore
    mpfr_init_set_ui(sum, 0, MPFR_RNDZ); // inizializzo a zero

    int cnt; // contatore ciclo
    mpq_t incq; // Incremento frazionario, aggiornato ad ogni ciclo
    mpq_init(incq);

    for (cnt = args.subdivision_start; cnt < args.subdivision_end; cnt++) {
        mpq_set_ui(incq, cnt, args.subdivisions); // inc = l * cnt / subdivisions
        mpfr_mul_q(inc, args.l, incq, MPFR_RNDN); // ottiene l'incremento l
        mpfr_add(x, args.a, inc, MPFR_RNDN); // Aggiorna la x
        va_copy(pass_params, args.params); // Una copia è indispensabile!
        args.f(y, x, pass_params); // Valuta la funzione in x
        va_end(pass_params);
        mpfr_add(sum, sum, y, MPFR_RNDN); // Aggiunge all'acumulatore
    }

    // Pulizia della memoria
    mpq_clear(incq);
    mpfr_clears(x, y, inc, NULL);

    mpfr_t * retval;
    retval = malloc(sizeof(*retval)); // Copia sull'heap dell'oggetto
    memcpy(retval, &sum, sizeof(sum)); // Esegue la copia
    mpfr_free_cache2(MPFR_FREE_LOCAL_CACHE);
    return retval; // Dopo il return sum viene distrutto, ma la sua copia sopravvive
}

int trapezi(mpfr_t res,
            const mpfr_srcptr a,
            const mpfr_srcptr b,
            int (*f)(mpfr_t res, const mpfr_srcptr x, const va_list params),
            int subdivisions,
            int n_threads,
             ... )
{
    va_list params, pass_params;
    va_start(params, n_threads); // Parametri variabili

    // calcolo subdivisions/n_threads arrotondato per eccesso
    int sub_per_thread = (subdivisions+n_threads-1)/n_threads;
    mpfr_t tres[n_threads]; // Array di tutti i risultati
    mpfr_ptr tresptr[n_threads]; // Array di tutti i risultati
    n_threads--; // Un thread è quello corrente
    pthread_t tid[n_threads]; // Array di thread da avviare
    args_t args[n_threads]; // Argomenti & risultato finale dei vari thread

    // Calcolo la lunghezza dell'intervallo
    mpfr_t l; // variabile che conterrà la lunghezza
    mpfr_init(l);
    mpfr_sub(l, b, a, MPFR_RNDN);  // Lunghezza dell'intervallo = b - a

    for (int i = 0; i < n_threads; i++) {
        args[i].subdivisions = subdivisions; // Numero totale di suddivisioni
        if (i != 0) // Per tutti gli altri thread prendo come start l'end del precedente
            args[i].subdivision_start = args[i-1].subdivision_end;
        else // Prima suddivisione, Comincia dalla suddivione 1 (la 0 è gestita dal main thread)
            args[i].subdivision_start = 1;
        args[i].subdivision_end = args[i].subdivision_start + sub_per_thread;
        if (args[i].subdivision_end > subdivisions) // Rara eventualità dovuta al roundup
            args[i].subdivision_end = subdivisions; // Cap al numero di suddivisioni
        va_copy(args[i].params, params); // Copia i parametri costanti
        args[i].a = a; args[i].l = l; args[i].f = f; // Uguali per tutti i thread
        pthread_create(&tid[i], NULL, trapezi_hlp, &args[i]); // Fa partire il thread
    }

    // Inizia qua, nel main thread, un altro integratore
    // che si preoccupa inoltre di gestire gli estremi
    mpfr_t x, y, inc; // somma temporanea, temporanei per la f, (a - x_i)
    mpfr_inits(x, y, inc, NULL);

    int cnt; // contatore ciclo
    mpq_t incq; // Incremento frazionario, aggiornato ad ogni ciclo
    mpq_init(incq);

    // Calcolo il 1° e l'ultimo valore
    mpfr_set(x, a, MPFR_RNDN); // Setto la x al 1° valore (estremo a)
    va_copy(pass_params, params); // Una copia è indispensabile!
    f(y, x, pass_params); // Calcola y come f(x)
    va_end(pass_params);
    mpfr_init_set(tres[0], y, MPFR_RNDN); // Aggiunge all'acumulatore, che vale 0

    mpfr_set(x, b, MPFR_RNDN); // Setto la x all'ultimo valore (estremo b)
    va_copy(pass_params, params); // Una copia è indispensabile!
    f(y, x, pass_params); // Calcola y come f(x)
    va_end(pass_params);
    mpfr_add(tres[0], tres[0], y, MPFR_RNDN); // Aggiunge all'acumulatore
    // Sia il primo che l'ultimo valore contano con peso 1/2, quindi divido per 2
    mpfr_div_2ui(tres[0], tres[0], 1, MPFR_RNDN); // Divide per 2

    int startfor = (n_threads != 0)?(args[n_threads-1].subdivision_end):1;
    for (cnt = startfor; cnt < subdivisions; cnt++) {
        mpq_set_ui(incq, cnt, subdivisions); // inc = l * cnt / subdivisions
        mpfr_mul_q(inc, l, incq, MPFR_RNDN); // ottiene l'incremento l
        mpfr_add(x, a, inc, MPFR_RNDN); // Aggiorna la x

        va_copy(pass_params, params); // Una copia è indispensabile!
        f(y, x, pass_params); // Valuta la funzione in x
        va_end(pass_params);

        mpfr_add(tres[0], tres[0], y, MPFR_RNDN); // Aggiunge all'acumulatore
    }

    // Pulizia della memoria
    mpq_clear(incq);
    mpfr_clears(x, y, inc, NULL);

    // Ora aspetta il joining di tutti i threads
    tresptr[0] = tres[0];
    for (int i = 0; i < n_threads; i++) {
        mpfr_t * temp;
        pthread_join(tid[i], (void**)&temp);
        memcpy(&tres[i+1], temp, sizeof(tres[i+1]));
        free(temp); // Libero la memoria dell'oggetto temporaneo
        va_end(args[i].params);
        tresptr[i+1] = tres[i+1];
    }

    // Non resta che sommare tutti i valori
    mpfr_t sum; // Una variabile per contenere la somma
    mpfr_init(sum);
    mpfr_sum(sum, tresptr, n_threads+1, MPFR_RNDN);

    // L'integrale sarà dato da  somma*(b-a)/subdivisions
    mpfr_mul(sum, sum, l, MPFR_RNDN);
    mpfr_div_ui(res, sum, subdivisions, MPFR_RNDN); // ora res contiene il risultato

    // Pulizia della memoria
    for (int i = 0; i < sizeof(tresptr)/sizeof(*tresptr); i++)
        mpfr_clear(tresptr[i]);
    mpfr_clears(l, sum, NULL);
    va_end(params); // Libera la memoria della lista argomenti
}

// ------- fine integratore --------

// Funzione da integrare sqrt(1 - k^2 sin^2) x
int f(mpfr_t res, const mpfr_srcptr x, const va_list params) {
    const mpfr_ptr alpha = va_arg(params, mpfr_ptr);
    const mpfr_ptr k_sq = va_arg(params, mpfr_ptr);
    mpfr_t res2, res3; // temporanee, per il calcolo di f
    mpfr_inits(res2, res3, NULL);

    mpfr_sin(res, x, MPFR_RNDN);
    mpfr_sqr(res2, res, MPFR_RNDN);
    mpfr_mul(res3, res2, k_sq, MPFR_RNDN);
    mpfr_ui_sub(res, 1, res3, MPFR_RNDN);

    mpfr_sqrt(res3, res, MPFR_RNDN);
    mpfr_rec_sqrt(res2, res, MPFR_RNDN);

    mpfr_fma(res, res3, alpha, res2, MPFR_RNDN);

    mpfr_clears(res2, res3, NULL);
}

double correction_factor(double Rbf, double Rf) {
    mpfr_set_default_prec(DEFAULT_PREC); // Imposto la precisione di default
    mpfr_t zero, halfpi, res; // Alcune costanti
    mpfr_t alpha, k_sqr, k; // Variabili del problema
    mpfr_t z, temp1, temp2; //Temporanee, per i calcoli di k e alpha

    mpfr_inits(res, halfpi, alpha, k_sqr, k, NULL);
    // Zero ed halfpi sono gli estremi di integrazione, inizializziamoli:
    mpfr_init_set_ui(zero, 0, MPFR_RNDZ); // Imposto zero al valore 0
    mpfr_set_d(res, INFINITY, MPFR_RNDA); // Imposto un temporaneo a +oo
    mpfr_atan(halfpi, res, MPFR_RNDN); // Ne calcolo l'arcotangente = pi/2

    // calcolo alpha = (Rb^2-R^2-z^2)/[(Rb-R)^2+z^2], z = Rb/2
    mpfr_init_set_d(z, Rbf, MPFR_RNDN); // z <-- Rb;
    mpfr_div_2ui(z, z, 1, MPFR_RNDN); // Divide per 2, z <-- z/2;
    mpfr_sqr(z, z, MPFR_RNDN); // z <-- z^2
    mpfr_init_set_d(temp1, Rf, MPFR_RNDN);
    mpfr_sqr(temp1, temp1, MPFR_RNDN); // R^2
    mpfr_init_set_d(temp2, Rf, MPFR_RNDN);
    mpfr_set_d(alpha, Rbf, MPFR_RNDN);
    mpfr_sub(temp2, alpha, temp2, MPFR_RNDN);
    mpfr_sqr(temp2, temp2, MPFR_RNDN);
    mpfr_add(temp2, temp2, z, MPFR_RNDN); // R^2
    mpfr_sqr(alpha, alpha, MPFR_RNDN); // R^2
    mpfr_sub(alpha, alpha, temp1, MPFR_RNDN);
    mpfr_sub(alpha, alpha, z, MPFR_RNDN);
    mpfr_div(alpha, alpha, temp2, MPFR_RNDN); // Calcolato alpha

    // calcolo k^2 = 4RbR/[(Rb+R)^2+z^2]
    mpfr_set_d(temp1, Rbf, MPFR_RNDN);
    mpfr_set_d(temp2, Rf, MPFR_RNDN);
    mpfr_mul(k, temp1, temp2, MPFR_RNDN);
    mpfr_add(temp2, temp2, temp1, MPFR_RNDN);
    mpfr_sqr(temp2, temp2, MPFR_RNDN);
    mpfr_add(temp2, temp2, z, MPFR_RNDN);
    mpfr_set_ui(temp1, 4, MPFR_RNDN);
    mpfr_mul(k, k, temp1, MPFR_RNDN);
    mpfr_div(k_sqr, k, temp2, MPFR_RNDN); // Calcolato k^2
    mpfr_sqrt(k, k_sqr, MPFR_RNDZ); // k = sqrt(k_sqr)
    mpfr_sqrt(temp2, temp2, MPFR_RNDZ); // k = sqrt(k_sqr)

    // mpfr_printf("alpha = %.7RNf\n", alpha);
    // mpfr_printf("k = %.7RNf\n", k_sqr);

    // Calcolo la costante moltiplicativa per R = 0
    mpfr_set_d(temp1, Rbf, MPFR_RNDN);
    mpfr_sqr(temp1, temp1, MPFR_RNDN);
    mpfr_add(temp1, temp1, z, MPFR_RNDN);
    mpfr_sqrt(temp1, temp1, MPFR_RNDZ); // costante moltiplicativa in temp1

    mpfr_mul_ui(res, halfpi, 16, MPFR_RNDN);
    mpfr_div_ui(res, res, 10, MPFR_RNDN); // carico 0.8 pi
    mpfr_div(temp1, temp1, res, MPFR_RNDN); // B(0)/(NImu0) in temp1

    trapezi(res, zero, halfpi, f, pow(10, 3), N_THREADS, alpha, k_sqr); // integra
    mpfr_div(res, res, temp2, MPFR_RNDN);
    mpfr_mul(res, res, temp1, MPFR_RNDN);
    // mpfr_printf("res = %.50RNf\n", res);
    double final_result = mpfr_get_d(res, MPFR_RNDN);
    mpfr_clears(z, temp1, temp2, NULL);

    mpfr_free_cache();
    mpfr_clears(res, zero, halfpi, alpha, k_sqr, k, NULL);

    return final_result;
}
