#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_sf_gamma.h>

double correction_factor(double Rbf, double Rf);
double error(double Rbf, double Rf);

int main(int argc, char * argv[]) {
    double Rb, Ns;
    double DeltaV, i, R;
    double mu0, c, B, cf;
    double incRb, incI, incB, inc, incR;
    double ascissa, ordinata;
    double * ascisse = NULL, * ordinate = NULL, * incertezze = NULL;
    int n = 0;
    double acc, k;

    // Per il calcolo della retta di regressione
    double wx, wy, wxy, wx2;
    double wsum, w;
    wsum = wx = wy = wxy = wx2 = 0;

    // Leggo le costanti dell'esperimento
    scanf("%lf", &Rb); // Raggio bobina
    scanf("%lf", &Ns); // Numero di spire
    scanf("%lf", &incRb); // incertezza sul raggio delle bobine
    scanf("%lf", &incR); // incertezza sul raggio
    scanf("%lf", &incI); // incertezza sulla corrente
    mu0 = 4*M_PI*1E-7;
    c = Ns*8*mu0/(5*sqrt(5))/Rb;
    while (!feof(stdin)) {
        if (scanf("%lf %lf %lf", &i, &DeltaV, &R) != 3) // Leggo le variabili
            break;
        // printf("%.3lf %.1lf %.4lf ", i, DeltaV, R);
        B = c*i;
        // printf("B(0) = %lf ", B);
        cf = correction_factor(Rb, R);
        // printf(" %lf ", cf);
        B *= cf;
        // printf("B(R) = %lf ", B);
        ascissa = 2*DeltaV;
        ordinata = pow(B*R, 2);
        ascisse = realloc(ascisse, (n+1)*sizeof(double));
        ordinate = realloc(ordinate, (n+1)*sizeof(double));
        ascisse[n] = ascissa;
        ordinate[n] = ordinata;
        // printf("Ordinata = %e ", ordinata);

        // Incertezza su B
        double after = correction_factor(Rb + incRb, R) - cf;
        double before = cf - correction_factor(Rb - incRb, R);
        double dcf_incRb = fabs(after) > fabs(before) ? after : before;
        after = correction_factor(Rb, R + incR) - cf;
        before = cf - correction_factor(Rb, R - incR);
        double dcf_incR = fabs(after) > fabs(before) ? after : before;
        incB = sqrt(pow(c*cf*incI, 2) + pow(-B/Rb*incRb + c*i*dcf_incRb, 2) + pow(c*i*dcf_incR, 2));
        // Incertezza ordinata
        inc = 2*sqrt(pow(B*R*R*incB, 2) + pow(R*B*B*incR, 2) + 8*pow(B*R, 3)*c*i*incR*dcf_incR);
        printf("%e %e %e", ascissa*1E-3, ordinata*1E9, inc*1E9);
        incertezze = realloc(incertezze, (n+1)*sizeof(double));
        incertezze[n] = inc;
        n++;

        w = pow(inc, -2);
        // printf("%e", w);
        wsum += w;
        wx += w*ascissa;
        wy += w*ordinata;
        wxy += w*ascissa*ordinata;
        wx2 += w*ascissa*ascissa;
        printf("\n");
    }

    // Retta NON passante per l'origine
    printf("%e, %e, %e, %e, %e\n", wsum, wx, wy, wxy, wx2);
    double delta = wsum*wx2-pow(wx, 2);
    double rB = (wsum*wxy-wy*wx)/delta;
    double rA = (wx2*wy-wx*wxy)/delta;
    printf("%e x + %e\n", rB, rA);
    printf("Radice %e\n", -rA/rB);
    double incrB = sqrt(wsum/delta);
    double eoverm = 1/rB;
    double inceoverm = incrB/pow(rB, 2);
    printf("e/m = %e , inc %e\n", eoverm, inceoverm);

    acc = 0;
    for (int i = 0; i < n; i++) {
        // Distanza ordinata dal valore atteso
        acc += pow((ordinate[i] - rB*ascisse[i] - rA)/incertezze[i], 2);
    }
    k = n-2; // Gradi di libertà
    printf("Numero dei dati: %d\n", n);
    printf("chi^2    = %e\n", acc);
    printf("chi^2/nu = %e\n", acc/k);

    printf("p = %lf %%\n", 100*gsl_sf_gamma_inc(k/2, acc/2)/tgamma(k/2));

    // Retta passante per l'origine
    rB = wxy/wx2;
    incrB = pow(wx2, -0.5);
    printf("y = %e x\n", rB);
    eoverm = 1/rB;
    inceoverm = incrB/pow(rB, 2);
    printf("e/m = %e , inc %e\n", eoverm, inceoverm);
    printf("Distanza valore vero: %e sigma\n", (1.75882002E11-eoverm)/inceoverm);

    // Test chi^2
    acc = 0;
    for (int i = 0; i < n; i++) {
        // Distanza ordinata dal valore atteso
        acc += pow((ordinate[i] - rB*ascisse[i])/incertezze[i], 2);
    }
    k = n-1; // Gradi di libertà
    printf("Numero dei dati: %d\n", n);
    printf("chi^2    = %e\n", acc);
    printf("chi^2/nu = %e\n", acc/k);

    printf("p = %lf %%\n", 100*gsl_sf_gamma_inc(k/2, acc/2)/tgamma(k/2));

    free(ascisse);
    free(ordinate);
    free(incertezze);
    return 0;
}